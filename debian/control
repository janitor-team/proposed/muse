Source: muse
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Fabrice Coutadeur <coutadeurf@gmail.com>,
 Dennis Braun <d_braun@kabelmail.de>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dssi-dev,
 imagemagick,
 libasound2-dev,
 libdssialsacompat-dev [!linux-any],
 libfluidsynth-dev,
 libinstpatch-dev,
 libjack-dev,
 liblilv-dev (>= 0.22),
 liblo-dev,
 libqt5svg5-dev,
 librtaudio-dev,
 librubberband-dev,
 libsamplerate0-dev,
 libsndfile1-dev,
 libsord-dev (>= 0.14),
 lv2-dev (>= 1.12),
 qtbase5-dev,
 qttools5-dev
Standards-Version: 4.6.0
Homepage: https://muse-sequencer.github.io/
Vcs-Git: https://salsa.debian.org/multimedia-team/muse.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/muse
Rules-Requires-Root: no

Package: muse
Architecture: any
Depends:
 python3,
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 jackd
Description: Qt-based audio/MIDI sequencer
 MusE is a MIDI/audio sequencer with recording and editing capabilities.
 Some Highlights:
 .
  * Standard midifile (smf) import-/export.
  * Organizes songs in tracks and parts which you can arrange with
    the part editor.
  * MIDI editors: pianoroll, drum, list, controller.
  * Score editor with high quality postscript printer output.
  * Realtime: editing while playing.
  * Unlimited number of open editors.
  * Unlimited undo/redo.
  * Realtime and step-recording.
  * Multiple MIDI devices.
  * Unlimited number of tracks.
  * Sync to external devices: MTC/MMC, Midi Clock, Master/Slave.
  * Audio tracks, LV2/DSSI/LADSPA host for master effects.
  * Multithreaded.
  * Uses raw MIDI devices.
  * XML project file.
  * Project file contains complete app state (session data).
  * Application spanning Cut/Paste Drag/Drop.
